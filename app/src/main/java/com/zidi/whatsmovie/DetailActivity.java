package com.zidi.whatsmovie;

import android.graphics.Color;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.DefaultSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.zidi.whatsmovie.adapter.RecyclerViewCollectionAdapter;
import com.zidi.whatsmovie.adapter.RecyclerViewTrailerAdapter;
import com.zidi.whatsmovie.model.BackDrop;
import com.zidi.whatsmovie.model.BelongsToCollection;
import com.zidi.whatsmovie.model.Cast;
import com.zidi.whatsmovie.model.Collection;
import com.zidi.whatsmovie.model.Crew;
import com.zidi.whatsmovie.model.Details;
import com.zidi.whatsmovie.model.Part;
import com.zidi.whatsmovie.model.ProductionCompany;
import com.zidi.whatsmovie.model.Videos;
import com.zidi.whatsmovie.rest.ApiClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity implements ViewPagerEx.OnPageChangeListener{

    public String movieId, movieTitle, collectionId;
    private TextView txtJudul, txtDeskripsi, txtVote, txtTahun, txtGenre;
    private ImageView imgMovie, imgProfpic;
    private Toolbar toolbar;
//    private TextView txtSementara;
    private TextView txtProduction, txtCast, txtCollection, txtFromCollection, txtTrailer, txtDirector;

    private SliderLayout mDemoSlider;

    List<ProductionCompany> listProductionCompany = new ArrayList<>();
    List<Cast> listCast = new ArrayList<>();
    List<Videos.Result> listTrailer = new ArrayList<>();
    List<BackDrop> listImages = new ArrayList<>();
    List<Crew> listCrew = new ArrayList<>();
    BelongsToCollection belongsToCollection = new BelongsToCollection();
    List<Part> listPart = new ArrayList<>();

    private RecyclerView mRecylcerView;
    private RecyclerViewTrailerAdapter mAdapter;

    private RecyclerView colRecyclerView;
    private RecyclerViewCollectionAdapter colAdapter;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mAdView = findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        initCollapsingToolbar();

        //TODO : get Intent's data
//        String latar = getIntent().getStringExtra("latar");
        String jud = getIntent().getStringExtra("judulOrigin");
        String des = getIntent().getStringExtra("deskripsi");
        String thn = getIntent().getStringExtra("release");
        String vote = getIntent().getStringExtra("vote");
        String poster = getIntent().getStringExtra("profpic");
        String judul = getIntent().getStringExtra("judul");
        String genre = getIntent().getStringExtra("genre");
        String voteCount = getIntent().getStringExtra("vote_count");
        this.movieId = getIntent().getStringExtra("id");
        this.movieTitle = judul;

        //TODO : Deklarasi Komponen
        txtJudul = (TextView) findViewById(R.id.tv_movie_title);
        txtDeskripsi = (TextView)findViewById(R.id.movie_desc);
        imgMovie = (ImageView)findViewById(R.id.img_movie_poster);
        imgMovie.setVisibility(View.GONE);
        txtTahun = (TextView)findViewById(R.id.movie_date);
        txtVote = (TextView)findViewById(R.id.movie_rate);
        imgProfpic = (ImageView)findViewById(R.id.movie_profpic);
        txtGenre = (TextView)findViewById(R.id.movie_genre);

        mDemoSlider = (SliderLayout)findViewById(R.id.slider);

        txtCast = (TextView) findViewById(R.id.movie_cast);
        txtCollection = (TextView) findViewById(R.id.movie_collection);
        txtFromCollection = (TextView) findViewById(R.id.tv_from_collection);
        txtTrailer = (TextView)findViewById(R.id.tv_trailer);
        txtDirector = (TextView) findViewById(R.id.movie_director);
        txtProduction = (TextView) findViewById(R.id.movie_production);


        //TODO : Inisialisasi Komponen
        Glide.with(this)
                .load(poster)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.blank_movie)
                        .error(R.drawable.blank_movie))
                .into(imgProfpic);

        txtJudul.setText(jud);
        txtDeskripsi.setText(des);
        txtTahun.setText("Release date : " + thn);
        txtVote.setText(vote + "/10 (" + voteCount + " votes)");
        txtGenre.setText(genre);

        mRecylcerView = (RecyclerView)findViewById(R.id.rv_trailer);
        mRecylcerView.setHasFixedSize(true);
        mRecylcerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerViewTrailerAdapter(getApplicationContext(),listTrailer);
        mRecylcerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        colRecyclerView = (RecyclerView)findViewById(R.id.rv_collection);
        colRecyclerView.setHasFixedSize(true);
        colRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        colAdapter = new RecyclerViewCollectionAdapter(getApplicationContext(),listPart);
        colRecyclerView.setAdapter(colAdapter);
        colAdapter.notifyDataSetChanged();

        //Call Movie's Detail
        requestMovieDetails();

    }

    private void doApapun() {
        //release_mode: Log.d("onCreate List","ProdCompany:"+listProductionCompany.size());
        //release_mode: Log.d("onCreate List","Cast:"+listCast.size());
        //release_mode: Log.d("onCreate List","Videos:"+listTrailer.size());
        //release_mode: Log.d("onCreate List","BackDrop:"+listImages.size());
        //release_mode: Log.d("onCreate List","Crew:"+listCrew.size());

//        if (!listImages.isEmpty()) {
        if (listImages.size() > 1) {
            //TODO : using Glide Slide Library
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .error(R.drawable.blank_movie);
            for (int idx = 0; idx < listImages.size(); idx++) {
                DefaultSliderView sliderView = new DefaultSliderView(this);
                sliderView
                        .image("https://image.tmdb.org/t/p/w780" + listImages.get(idx).getFilePath())
                        .setRequestOption(requestOptions)
                        .setBackgroundColor(Color.BLACK);
//                        .setProgressBarVisible(true);
                mDemoSlider.addSlider(sliderView);
            }
            if (listImages.size() == 1){
                mDemoSlider.stopAutoCycle();
            }
            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Stack);
            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            mDemoSlider.setDuration(4000);
            mDemoSlider.addOnPageChangeListener(this);
        } else if (listImages.size() == 1) {
            imgMovie.setVisibility(View.VISIBLE);
            String latar = "https://image.tmdb.org/t/p/w780" + listImages.get(0).getFilePath();
            Glide.with(this)
                    .load(latar)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.blank_movie)
                            .error(R.drawable.blank_movie))
                    .into(imgMovie);

        }
        else {
            imgMovie.setVisibility(View.VISIBLE);
            String latar = getIntent().getStringExtra("latar");
            Glide.with(this)
                    .load(latar)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.blank_movie)
                            .error(R.drawable.blank_movie))
                    .into(imgMovie);
        }

        if (!listProductionCompany.isEmpty()) {
            //TODO : Get info detail lainnya
            //COMPANY PRODUCTION
            String prodCompany = ""; String prefix = ""; int idx1 = 0;
            StringBuilder sb1 = new StringBuilder();
            do{
                prodCompany = listProductionCompany.get(idx1).getName();
                sb1.append(prefix);
                prefix = ", ";
                sb1.append(prodCompany);
                idx1++;
            } while (idx1<listProductionCompany.size());
            txtProduction.setText(sb1.toString());
        }

        if (!listCast.isEmpty()) {
            //CAST
            String cast=""; String prefix=""; int idx2 = 0;
            StringBuilder sb2 = new StringBuilder();
            do {
                cast = listCast.get(idx2).getName();
                sb2.append(prefix);
                prefix = ", ";
                sb2.append(cast);
                idx2++;
            } while (idx2<listCast.size() && idx2 <= 10);
            txtCast.setText(sb2.toString());
        }

        if (!listCrew.isEmpty()) {
            //DIRECTOR
            String director = "";
            for (int i = 0; i < listCrew.size(); i++) {
                if (listCrew.get(i).getJob().equals("Director")) {
                    director += listCrew.get(i).getName();
                    break;
                }
            }
            txtDirector.setText(director);
        }

        if (belongsToCollection != null) {
            //COLLECTION
            String collection = "";
            collection = belongsToCollection.getName();
            txtFromCollection.setVisibility(View.VISIBLE);
            txtCollection.setText(collection);
            this.collectionId = String.valueOf(belongsToCollection.getId());
            //release_mode: Log.d("Detail",this.collectionId);
            requestCollection();
        }

        if (listTrailer.isEmpty()){
            txtTrailer.setVisibility(View.VISIBLE);
        }
    }

    private void requestCollection() {
        try{
            ApiClient.get().getCollection(collectionId,getString(R.string.api_key))
                    .enqueue(new Callback<Collection>() {
                        @Override
                        public void onResponse(Call<Collection> call, Response<Collection> response) {
                            List<Part> parts = response.body().getParts();
                            //release_mode: Log.d("Detail","masuk ke requestCollection");
                            //release_mode: Log.d("hasil JSON",parts.toString());
                            listPart.addAll(parts);
                            colAdapter.notifyDataSetChanged();

                        }

                        @Override
                        public void onFailure(Call<Collection> call, Throwable t) {
                            //release_mode: Log.d("ERROR", t.getMessage());
                            Toast.makeText(DetailActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });
        } catch (Exception e){
            //release_mode: Log.d("ERROR", e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("");

        final AppBarLayout appBar = (AppBarLayout) findViewById(R.id.app_bar);
        appBar.setExpanded(true);

        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isVisible = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (scrollRange == -1) {
                    scrollRange = appBar.getTotalScrollRange();
                }

                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(movieTitle);
                    isVisible = true;
                } else if (isVisible) {
                    collapsingToolbar.setTitle("");
                    isVisible = false;
                }
            }
        });
    }

    private void requestMovieDetails(){
        try{
            ApiClient.get().getDetailsMovie(movieId, getString(R.string.api_key),"videos,images,credits")
                    .enqueue(new Callback<Details>(){
                       @Override
                        public void onResponse(Call<Details> call, Response<Details> response){
                           Details hasil = (Details)response.body();
                           List<ProductionCompany> productionCompanies = response.body().getProductionCompanies();
                           List<Cast> credits = response.body().getCredits().getCast();
                           List<Videos.Result> videos = response.body().getVideos().getResults();
                           List<BackDrop> images = response.body().getImages().getBackdrops();
                           List<Crew> crew = response.body().getCredits().getCrew();
                           belongsToCollection = response.body().getBelongsToCollection();
                           listProductionCompany.addAll(productionCompanies);
                           listCast.addAll(credits);
                           listTrailer.addAll(videos);
                           mAdapter.notifyDataSetChanged();
                           listImages.addAll(images);
                           listCrew.addAll(crew);
                           //release_mode: Log.d("isian List","ProdCompany:"+listProductionCompany.size());
                           //release_mode: Log.d("isian List","Cast:"+listCast.size());
                           //release_mode: Log.d("isian List","Trailer:"+listTrailer.size());
                           //release_mode: Log.d("isian List","BackDrop:"+listImages.size());
                           //release_mode: Log.d("isian List","Crew:"+listCrew.size());

                           doApapun();
                       }

                       @Override
                        public void onFailure(Call<Details> call, Throwable t){
                           //release_mode: Log.d("ERROR", t.getMessage());
                           Toast.makeText(DetailActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                       }
                    });

        } catch (Exception e){
            //release_mode: Log.d("ERROR", e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    protected void onStop() {
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }
}
