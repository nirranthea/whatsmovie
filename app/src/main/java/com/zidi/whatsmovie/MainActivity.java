package com.zidi.whatsmovie;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.zidi.whatsmovie.adapter.RecyclerViewMovieAdapter;
import com.zidi.whatsmovie.model.Movie;
import com.zidi.whatsmovie.model.MovieResult;
import com.zidi.whatsmovie.rest.ApiClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RecyclerViewMovieAdapter mAdapter;
    private NavigationView navigationView;
    private Switch mSwitch;
    protected Handler handler;
    private int page, mode, totalPages;
    private String region="";
    private String nowDate, beforeDate, tomorrowDate;

    List<MovieResult> listMovies = new ArrayList<>();

    public final static String ADMOB_APP_ID = "ca-app-pub-1749663308384271/8963153473";
    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer);

        //TODO: my own Admob
        MobileAds.initialize(this, ADMOB_APP_ID);
        mAdView = findViewById(R.id.adView1);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        //Get current Date
        Date mDate = Calendar.getInstance().getTime();
        Calendar calAgo = Calendar.getInstance();
        calAgo.add(Calendar.MONTH,-1);
        Date mDateAgo = calAgo.getTime();
        Calendar calTom = Calendar.getInstance();
        calTom.add(Calendar.DATE,1);
        Date mDateTom = calTom.getTime();

        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        nowDate = formatDate.format(mDate);
        beforeDate = formatDate.format(mDateAgo);
        tomorrowDate = formatDate.format(mDateTom);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        // check screen orientation
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            // if portrait
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            // if landscape
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        }

        mAdapter = new RecyclerViewMovieAdapter(getApplicationContext(),listMovies,mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Scroll to Top", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                mRecyclerView.smoothScrollToPosition(0);
            }
        });

        mSwitch = (Switch) findViewById(R.id.switch1);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        if (savedInstanceState != null){
            mode = savedInstanceState.getInt("mode");
            //release_mode: Log.d("mode ambil:",String.valueOf(mode));
        }
        else {
            mode = 0;
            //release_mode: Log.d("mode diisi:",String.valueOf(mode));
        }
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(mode).setChecked(true); //default

        initViews(mode);

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_orange_dark);

        onSwipeRefresh();

    }

    private void onSwipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initViews(mode);
                navigationView.getMenu().getItem(0).setChecked(true);
            }
        });
    }

    private void initViews(final int mode) {
        handler = new Handler();

        if (mode == 0){
            getSupportActionBar().setTitle("Now Playing");
        }
        else if (mode == 1){
            getSupportActionBar().setTitle("Coming Soon");
        }
        else {
            getSupportActionBar().setTitle("Whats Movie");
        }

        listMovies.clear();
        page = 1;
        mAdapter.notifyDataSetChanged();

        //Call request Movie method
        requestMovieDatabase(page,mode,region);
        //release_mode: Log.d("Jumlah list",String.valueOf(listMovies.size()));
        //release_mode: Log.d("number page",String.valueOf(page));
        page++;

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        MovieResult tesAjah = new MovieResult();
//                        tesAjah.setTitle("tes ajah");
//                        tesAjah.setVoteAverage(3.5);
//                        listMovies.add(tesAjah);
                        if (page <= totalPages) {
                            requestMovieDatabase(page, mode, region);
                            //release_mode: Log.d("Jumlah list", String.valueOf(listMovies.size()));
                            //release_mode: Log.d("number page", String.valueOf(page));
                            page++;
                            mAdapter.setLoaded();
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                },1000);
            }
        });

        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //release_mode: Log.v("Switch State=", ""+isChecked);
                if (isChecked){
                    region = "id";
                    initViews(mode);
                }
                else {
                    region = "";
                    initViews(mode);
                }
            }
        });
    }

    //TODO 9 : create method to request Movie DataBase
    private void requestMovieDatabase(int page, int mode, String region) {
        try {
            switch (mode){
                case 0:
                    ApiClient.get().getNowPlaying("movie",getString(R.string.api_key), String.valueOf(page),nowDate,beforeDate,region)
                            .enqueue(new Callback<Movie>() {
                                @Override
                                public void onResponse(Call<Movie> call, Response<Movie> response) {
                                    totalPages = ((Movie)response.body()).getTotalPages();
                                    List<MovieResult> movies = response.body().getResults();
                                    listMovies.addAll(movies);
                                    mAdapter.notifyDataSetChanged();
                                    mAdapter.setLoaded();
                                    if (mSwipeRefreshLayout.isRefreshing()) {
                                        mSwipeRefreshLayout.setRefreshing(false);
                                    }
                                    //release_mode: Toast.makeText(MainActivity.this, "Showing movie database", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailure(Call<Movie> call, Throwable t) {
                                    //release_mode: Log.d("ERROR", t.getMessage());
                                    if (mSwipeRefreshLayout.isRefreshing()) {
                                        mSwipeRefreshLayout.setRefreshing(false);
                                    }
                                    Toast.makeText(MainActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            });
                    break;
                case 1:
                    ApiClient.get().getComingSoon("movie",getString(R.string.api_key), String.valueOf(page),tomorrowDate,region)
                            .enqueue(new Callback<Movie>() {
                                @Override
                                public void onResponse(Call<Movie> call, Response<Movie> response) {
                                    totalPages = ((Movie)response.body()).getTotalPages();
                                    List<MovieResult> movies = response.body().getResults();
                                    listMovies.addAll(movies);
                                    mAdapter.notifyDataSetChanged();
                                    mAdapter.setLoaded();
                                    if (mSwipeRefreshLayout.isRefreshing()) {
                                        mSwipeRefreshLayout.setRefreshing(false);
                                    }
                                    //release_mode: Toast.makeText(MainActivity.this, "Showing movie database", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailure(Call<Movie> call, Throwable t) {
                                    //release_mode: Log.d("ERROR", t.getMessage());
                                    if (mSwipeRefreshLayout.isRefreshing()) {
                                        mSwipeRefreshLayout.setRefreshing(false);
                                    }
                                    Toast.makeText(MainActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            });
                    break;
                    default:
                        break;
            }

        } catch (Exception e){
            //release_mode: Log.d("ERROR", e.getMessage());
            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                Intent aboutIntent = new Intent(MainActivity.this,AboutActivity.class);
                startActivity(aboutIntent);
                break;
            case R.id.action_search:
                Intent searchIntent = new Intent(MainActivity.this,SearchActivity.class);
                startActivity(searchIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_now_playing) {
            // Handle the now playing action
            mode = 0;
            //release_mode: Log.d("mode nav:",String.valueOf(mode));
            initViews(mode);
        } else if (id == R.id.nav_coming_soon) {
            // Handle the coming soon action
            mode = 1;
            //release_mode: Log.d("mode nav:",String.valueOf(mode));
            initViews(mode);
        } else if (id == R.id.nav_discover) {
            // Handle the discover action
            Intent discoverIntent = new Intent(MainActivity.this,DiscoverActivity.class);
            startActivity(discoverIntent);
        } else if (id == R.id.nav_about) {
            Intent aboutIntent = new Intent(MainActivity.this,AboutActivity.class);
            startActivity(aboutIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //release_mode: Log.d("mode Instance:",String.valueOf(mode));
        outState.putInt("mode",mode);
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(mode).setChecked(true); //last position
    }
}
