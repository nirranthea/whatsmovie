package com.zidi.whatsmovie.rest;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by zidi on 06/06/2018.
 */
//TODO 5 : create API Client
public class ApiClient {
    public static Retrofit retrofit = null;
    public static String BASE_URL = "https://api.themoviedb.org/3/";

    public static ApiInterface get(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.SECONDS)
                .writeTimeout(4,TimeUnit.SECONDS)
                .readTimeout(4,TimeUnit.SECONDS)
                .addInterceptor(logging).build();

        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit.create(ApiInterface.class);
    }
}
