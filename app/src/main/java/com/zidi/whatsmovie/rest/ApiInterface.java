package com.zidi.whatsmovie.rest;

import com.zidi.whatsmovie.model.Collection;
import com.zidi.whatsmovie.model.Details;
import com.zidi.whatsmovie.model.Movie;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by zidi on 06/06/2018.
 */
//TODO 6 : create API Interface
public interface ApiInterface {

    @GET("https://api.themoviedb.org/3/discover/{movie_id}")
    Call<Movie> getNowPlaying(@Path("movie_id") String movie_id, @Query("api_key") String api_key, @Query("page") String page,
                              @Query("primary_release_date.lte") String date_lte, @Query("primary_release_date.gte") String date_gte,
                              @Query("region") String region);

    @GET("https://api.themoviedb.org/3/discover/{movie_id}")
    Call<Movie> getComingSoon(@Path("movie_id") String movie_id, @Query("api_key") String api_key, @Query("page") String page,
                              @Query("primary_release_date.gte") String date_gte,
                              @Query("region") String region);

    @GET("https://api.themoviedb.org/3/search/{movie_id}")
    Call<Movie> getSearchMovie(@Path("movie_id") String movie_id, @Query("api_key") String api_key, @Query("page") String page,
                              @Query("query") String query);

    @GET("https://api.themoviedb.org/3/discover/{movie_id}")
    Call<Movie> getGenreMovie(@Path("movie_id") String movie_id, @Query("api_key") String api_key, @Query("page") String page,
                               @Query("with_genres") String genre, @Query("primary_release_year") String year);

    @GET("https://api.themoviedb.org/3/movie/{movie_id}")
    Call<Details> getDetailsMovie(@Path("movie_id") String movie_id, @Query("api_key") String api_key,
                                  @Query("append_to_response") String append);

    @GET("https://api.themoviedb.org/3/collection/{movie_id}")
    Call<Collection> getCollection(@Path("movie_id") String movie_id, @Query("api_key") String api_key);
}
