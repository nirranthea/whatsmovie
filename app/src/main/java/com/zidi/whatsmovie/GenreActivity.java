package com.zidi.whatsmovie;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.zidi.whatsmovie.adapter.RecyclerViewMovieAdapter;
import com.zidi.whatsmovie.model.Movie;
import com.zidi.whatsmovie.model.MovieResult;
import com.zidi.whatsmovie.rest.ApiClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenreActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private RecyclerView mRecyclerView;
    private RecyclerViewMovieAdapter mAdapter;
    private int page,totalPages;
    private String genre,genreCode;
    private Handler handler;
    private String inputYear;
    
    List<MovieResult> listMovies = new ArrayList<>();
    String years[] = {"2018", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010"};
    String[] years_array;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);

        mAdView = findViewById(R.id.adView4);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        years_array = getResources().getStringArray(R.array.years_array);
        Spinner spYear = (Spinner)findViewById(R.id.spinner_year);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,years_array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spYear.setAdapter(adapter);
        spYear.setOnItemSelectedListener(this);

        genreCode = getIntent().getStringExtra("genreCode");
        genre = getIntent().getStringExtra("genre");
        getSupportActionBar().setTitle(genre);

        //release_mode: Log.d("genre:",genre);
        //release_mode: Log.d("genreCode:",String.valueOf(genreCode));

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_genre);
        mRecyclerView.setHasFixedSize(true);
        // check screen orientation
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            // if portrait
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            // if landscape
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Scroll to Top", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                mRecyclerView.smoothScrollToPosition(0);
            }
        });

        handler = new Handler();
        listMovies.clear();
        page = 1;
        inputYear = years[0]; //initialize
        mAdapter = new RecyclerViewMovieAdapter(getApplicationContext(),listMovies,mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

//        requestMovieGenre(page,genreCode,inputYear);
//        Log.d("Jumlah list",String.valueOf(listMovies.size()));
//        Log.d("number page",String.valueOf(page));
//        page++;

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//
                        if (page <= totalPages) {
                            requestMovieGenre(page, genreCode,inputYear);
                            //release_mode: Log.d("Jumlah list", String.valueOf(listMovies.size()));
                            //release_mode: Log.d("load more number page", String.valueOf(page));
                            page++;
                            mAdapter.setLoaded();
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                },1000);
            }
        });
    }

    private void requestMovieGenre(final int page, String genreCode, String inputYear) {
        try{
            ApiClient.get().getGenreMovie("movie",getString(R.string.api_key), String.valueOf(page),genreCode,inputYear)
                    .enqueue(new Callback<Movie>() {
                        @Override
                        public void onResponse(Call<Movie> call, Response<Movie> response) {
                            totalPages = ((Movie)response.body()).getTotalPages();
                            List<MovieResult> movies = response.body().getResults();
                            listMovies.addAll(movies);
                            mAdapter.notifyDataSetChanged();
                            mAdapter.setLoaded();
                            //release_mode: Toast.makeText(GenreActivity.this, "Showing page" + String.valueOf(page), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Movie> call, Throwable t) {
                            //release_mode: Log.d("ERROR", t.getMessage());
                            Toast.makeText(GenreActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });

        } catch (Exception e){
            //release_mode: Log.d("ERROR", e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        menu.getItem(1).setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                Intent aboutIntent = new Intent(GenreActivity.this,AboutActivity.class);
                startActivity(aboutIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (position){
            case 0:
                inputYear = years_array[0];
                break;
            case 1:
                inputYear = years_array[1];
                break;
            case 2:
                inputYear = years_array[2];
                break;
            case 3:
                inputYear = years_array[3];
                break;
            case 4:
                inputYear = years_array[4];
                break;
            case 5:
                inputYear = years_array[5];
                break;
            case 6:
                inputYear = years_array[6];
                break;
            case 7:
                inputYear = years_array[7];
                break;
            case 8:
                inputYear = years_array[8];
                break;
            default:
                inputYear = "2018";
                break;
        }
        //release_mode: Toast.makeText(GenreActivity.this,"Spinner:"+inputYear,Toast.LENGTH_SHORT).show();
        listMovies.clear();
        page = 1;
        requestMovieGenre(page,genreCode,inputYear);
        //release_mode: Log.d("Jumlah list", String.valueOf(listMovies.size()));
        //release_mode: Log.d("spinner number page", String.valueOf(page));
        page++;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
            inputYear = "2018";
    }
}
