package com.zidi.whatsmovie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.zidi.whatsmovie.adapter.RecyclerViewGenreAdapter;

import java.util.ArrayList;
import java.util.List;

public class DiscoverActivity extends AppCompatActivity {

    private RecyclerView gRecyclerView;
    private RecyclerViewGenreAdapter gAdapter;
    private List<String> genreList;
    private List<String> genreCodeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover);

        getSupportActionBar().setTitle("Discover");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gRecyclerView = (RecyclerView)findViewById(R.id.rv_discover);
        gRecyclerView.setHasFixedSize(true);
        gRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        genreList = new ArrayList<>();
        genreList.addAll(GenresData.getArrayListGenres());
        genreCodeList = new ArrayList<>();
        genreCodeList.addAll(GenresData.getArrayListCodeGenres());
        gAdapter = new RecyclerViewGenreAdapter(this,genreList,genreCodeList);
        gRecyclerView.setAdapter(gAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
