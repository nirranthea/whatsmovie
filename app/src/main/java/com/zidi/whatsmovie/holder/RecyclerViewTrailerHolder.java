package com.zidi.whatsmovie.holder;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.zidi.whatsmovie.R;

public class RecyclerViewTrailerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tvTrailer;
    public String keyTrailer;

    public RecyclerViewTrailerHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        tvTrailer = (TextView) itemView.findViewById(R.id.itm_trailer);
    }

    @Override
    public void onClick(View view) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + keyTrailer));
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + keyTrailer));

        try {
            view.getContext().startActivity(appIntent);
        } catch (ActivityNotFoundException ex){
            view.getContext().startActivity(webIntent);
        }
    }
}
