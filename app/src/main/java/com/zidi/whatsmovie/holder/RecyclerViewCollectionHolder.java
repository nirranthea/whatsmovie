package com.zidi.whatsmovie.holder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zidi.whatsmovie.DetailActivity;
import com.zidi.whatsmovie.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RecyclerViewCollectionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txtCollection;
    public ImageView imgCollection;

    public String original_title, id_film, poster, backdrop, release, vote, vote_count, genre, overview;

    public RecyclerViewCollectionHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        txtCollection = (TextView)itemView.findViewById(R.id.tv_collection);
        imgCollection = (ImageView)itemView.findViewById(R.id.img_collection);
    }

    @Override
    public void onClick(View v) {
        Intent collectIntent = new Intent(v.getContext(),DetailActivity.class);
        collectIntent.putExtra("id",id_film);
        collectIntent.putExtra("judulOrigin",original_title);
        collectIntent.putExtra("judul",txtCollection.getText().toString());
        collectIntent.putExtra("deskripsi",overview);
        collectIntent.putExtra("release",convertDate(release));
        collectIntent.putExtra("latar",backdrop);
        collectIntent.putExtra("profpic",poster);
        collectIntent.putExtra("genre",genre);
        collectIntent.putExtra("vote",vote);
        collectIntent.putExtra("vote_count",vote_count);
        v.getContext().startActivity(collectIntent);
    }

    public String convertDate(String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try{
            myDate = dateFormat.parse(date);
        } catch (ParseException e){
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM yyy");
        String finalDate = timeFormat.format(myDate);
        return finalDate;
    }
}
