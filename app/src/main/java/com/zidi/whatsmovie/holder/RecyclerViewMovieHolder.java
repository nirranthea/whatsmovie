package com.zidi.whatsmovie.holder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zidi.whatsmovie.DetailActivity;
import com.zidi.whatsmovie.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zidi on 06/06/2018.
 */
// TODO 7 : create Holder for Recycler View
public class RecyclerViewMovieHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView title;
    public ImageView image;
    public TextView rating;

    public String original_title, id_film, poster, backdrop, release, vote, vote_count, genre, overview;

    public RecyclerViewMovieHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        title =(TextView)itemView.findViewById(R.id.movie_title);
        rating = (TextView)itemView.findViewById(R.id.movie_star);
        image = (ImageView)itemView.findViewById(R.id.movie_thumbnail);
    }

    @Override
    public void onClick(View v) {
        //TODO next: Define intent for DetailActivity
        Intent detailIntent = new Intent(v.getContext(), DetailActivity.class);

        detailIntent.putExtra("id",id_film);
        detailIntent.putExtra("judulOrigin",original_title);
        detailIntent.putExtra("judul",title.getText().toString());
        detailIntent.putExtra("deskripsi",overview);
        detailIntent.putExtra("release",convertDate(release));
        detailIntent.putExtra("latar",backdrop);
        detailIntent.putExtra("profpic",poster);
        detailIntent.putExtra("genre",genre);
        detailIntent.putExtra("vote",vote);
        detailIntent.putExtra("vote_count",vote_count);

        v.getContext().startActivity(detailIntent);
    }

    public String convertDate(String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try{
            myDate = dateFormat.parse(date);
        } catch (ParseException e){
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM yyy");
        String finalDate = timeFormat.format(myDate);
        return finalDate;
    }
}
