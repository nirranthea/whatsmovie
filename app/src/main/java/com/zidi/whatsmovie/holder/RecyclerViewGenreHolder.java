package com.zidi.whatsmovie.holder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zidi.whatsmovie.GenreActivity;
import com.zidi.whatsmovie.R;

public class RecyclerViewGenreHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView mGenre;
    public String codeGenre;
    public ImageView mIcon;

    public RecyclerViewGenreHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        mGenre = (TextView)itemView.findViewById(R.id.tv_genre);
        mIcon = (ImageView)itemView.findViewById(R.id.iv_genre);
    }

    @Override
    public void onClick(View v) {
        //TODO next: Define intent for AnotherActivity
        Intent genreIntent = new Intent(v.getContext(), GenreActivity.class);
        genreIntent.putExtra("genre",mGenre.getText().toString());
        genreIntent.putExtra("genreCode",codeGenre);
        v.getContext().startActivity(genreIntent);
    }
}
