package com.zidi.whatsmovie;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zidi on 19/05/2018.
 */

//TODO : Create Array to define genre's code
public class GenresData {
    public static String[] [] genres = new String[][]{
            {"28","Action"},
            {"12","Adventure"},
            {"16","Animation"},
            {"35","Comedy"},
            {"80","Crime"},
            {"99","Documentary"},
            {"18","Drama"},
            {"10751","Family"},
            {"14","Fantasy"},
            {"36","History"},
            {"27","Horror"},
            {"10402","Music"},
            {"9648","Mystery"},
            {"10749","Romance"},
            {"878","Science Fiction"},
            {"10770","TV Movie"},
            {"53","Thriller"},
            {"10752","War"},
            {"37","Western"}
    };

    public static HashMap<Integer,String> getListGenres(){
        HashMap<Integer,String> mapGenre = new HashMap<Integer, String>();
        for (int i=0; i<genres.length; i++){
            mapGenre.put(Integer.parseInt(genres[i][0]),genres[i][1]);
        }
        return mapGenre;
    }

    public static ArrayList<String> getArrayListGenres(){
        ArrayList<String> arrayGenre = new ArrayList<>();
        for (int i=0; i<genres.length; i++){
            arrayGenre.add(i,genres[i][1]);
        }
        return arrayGenre;
    }

    public static ArrayList<String> getArrayListCodeGenres(){
        ArrayList<String> arrayCodeGenre = new ArrayList<>();
        for (int i=0; i<genres.length; i++){
            arrayCodeGenre.add(i,genres[i][0]);
        }
        return arrayCodeGenre;
    }

}
