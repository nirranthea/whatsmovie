package com.zidi.whatsmovie;

import android.app.SearchManager;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.zidi.whatsmovie.adapter.RecyclerViewMovieAdapter;
import com.zidi.whatsmovie.model.Movie;
import com.zidi.whatsmovie.model.MovieResult;
import com.zidi.whatsmovie.rest.ApiClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

//    private LinearLayout mLinearLayout;
    private RecyclerView mRecyclerView;
    private RecyclerViewMovieAdapter mAdapter;
    private int page,mode,totalPages;
    private String inputSearch = "";
    protected Handler handler, shandler;

    List<MovieResult> listMovies = new ArrayList<>();

    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        mAdView = findViewById(R.id.adView3);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

//        mLinearLayout = (LinearLayout) findViewById(R.id.search_layout);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_search);
        mRecyclerView.setHasFixedSize(true);

        // check screen orientation
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            // if portrait
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            // if landscape
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        }
        handler = new Handler();
        shandler = new Handler();
        listMovies.clear();
        page = 1;
        mAdapter = new RecyclerViewMovieAdapter(getApplicationContext(),listMovies,mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//
                        if (page <= totalPages) {
                            requestMovieSearch(page, inputSearch);
                            //release_mode: Log.d("Jumlah list", String.valueOf(listMovies.size()));
                            //release_mode: Log.d("load more number page", String.valueOf(page));
                            page++;
                            mAdapter.setLoaded();
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                },1000);
            }
        });
    }

    private void requestMovieSearch(final int page, String query) {
        try{
            ApiClient.get().getSearchMovie("movie",getString(R.string.api_key), String.valueOf(page),query)
                .enqueue(new Callback<Movie>() {
                    @Override
                    public void onResponse(Call<Movie> call, Response<Movie> response) {
                        totalPages = ((Movie)response.body()).getTotalPages();
                        List<MovieResult> movies = response.body().getResults();
                        listMovies.addAll(movies);
                        mAdapter.notifyDataSetChanged();
                        mAdapter.setLoaded();
                        //release_mode: Toast.makeText(SearchActivity.this, "Showing page" + String.valueOf(page), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Movie> call, Throwable t) {
                        //release_mode: Log.d("ERROR", t.getMessage());
                        Toast.makeText(SearchActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        } catch (Exception e){
            //release_mode: Log.d("ERROR", e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_menu, menu);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //TODO : searching function
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_movie_searching).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        //TODO : about keyboard
//        searchView.setIconifiedByDefault(true);
        searchView.setIconifiedByDefault(false);
//        searchView.setFocusable(true);
        searchView.setIconified(false);
//        searchView.requestFocusFromTouch();
        searchView.setQueryHint("Search Movies...");

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(getBaseContext(),"ambil search "+query,Toast.LENGTH_LONG).show();
                searchView.clearFocus();

                listMovies.clear();
                page = 1;
                mAdapter.notifyDataSetChanged();
                requestMovieSearch(page,query);
                //release_mode: Log.d("Jumlah list", String.valueOf(listMovies.size()));
                //release_mode: Log.d("submit number page", String.valueOf(page));
                page++;
                inputSearch = query;

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length()!=0) {
                    searchView.setIconifiedByDefault(false);
//                    searchView.setFocusable(true);
                    searchView.setIconified(false);

                    inputSearch = newText;
                    shandler.removeCallbacksAndMessages(null);
                    shandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            listMovies.clear();
                            page = 1;
                            mAdapter.notifyDataSetChanged();
                            requestMovieSearch(page,inputSearch);
                            //release_mode: Log.d("Jumlah list", String.valueOf(listMovies.size()));
                            //release_mode: Log.d("change number page", String.valueOf(page));
                            page++;

                        }
                    },1000);

                }
                else {
                    searchView.setIconifiedByDefault(false);
                }


                return true;
            }

        };
        searchView.setOnQueryTextListener(queryTextListener);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
