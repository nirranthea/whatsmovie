package com.zidi.whatsmovie.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zidi.whatsmovie.GenresData;
import com.zidi.whatsmovie.R;
import com.zidi.whatsmovie.holder.RecyclerViewCollectionHolder;
import com.zidi.whatsmovie.model.Part;

import java.util.HashMap;
import java.util.List;

public class RecyclerViewCollectionAdapter extends RecyclerView.Adapter<RecyclerViewCollectionHolder>{

    public Context context;
    public List<Part> partList;

    public RecyclerViewCollectionAdapter(Context context, List<Part> partList) {
        this.context = context;
        this.partList = partList;
    }

    @NonNull
    @Override
    public RecyclerViewCollectionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_collection,null);
        RecyclerViewCollectionHolder holder = new RecyclerViewCollectionHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewCollectionHolder holder, int position) {
        holder.txtCollection.setText(partList.get(position).getTitle());
        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w185"+partList.get(position).getPosterPath())
                .apply(new RequestOptions()
                .placeholder(R.drawable.movie_blank_portrait)
                .error(R.drawable.movie_blank_portrait))
                .into(holder.imgCollection);

        holder.id_film = partList.get(position).getId().toString();
        holder.original_title = partList.get(position).getOriginalTitle();
        holder.overview = partList.get(position).getOverview();
        holder.release = partList.get(position).getReleaseDate();
        holder.poster = "https://image.tmdb.org/t/p/w185" + String.valueOf(partList.get(position).getPosterPath());
        holder.vote = partList.get(position).getVoteAverage().toString();
        holder.vote_count = partList.get(position).getVoteCount().toString();

        HashMap<Integer,String> mapGenre = GenresData.getListGenres();
        int genre_id; String nameGenre; String prefix = "";
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<partList.get(position).getGenreIds().size();i++){
            genre_id = partList.get(position).getGenreIds().get(i);
            nameGenre = mapGenre.get(genre_id);
            sb.append(prefix);
            prefix = ", ";
            sb.append(nameGenre);
        }
        holder.genre = sb.toString();

    }

    @Override
    public int getItemCount() {
        return this.partList.size();
    }
}
