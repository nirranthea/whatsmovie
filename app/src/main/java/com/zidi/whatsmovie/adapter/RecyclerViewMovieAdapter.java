package com.zidi.whatsmovie.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zidi.whatsmovie.GenresData;
import com.zidi.whatsmovie.OnLoadMoreListener;
import com.zidi.whatsmovie.R;
import com.zidi.whatsmovie.holder.RecyclerViewMovieHolder;
import com.zidi.whatsmovie.model.MovieResult;

import java.util.HashMap;
import java.util.List;

/**
 * Created by zidi on 06/06/2018.
 */
//TODO 8 : create RecyclerView Adapter extends RecyclerView Holder for Movie
public class RecyclerViewMovieAdapter extends RecyclerView.Adapter<RecyclerViewMovieHolder>{

    private Context context;
    private List<MovieResult> movieList;

    //for load more
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;


    public RecyclerViewMovieAdapter(Context context, List<MovieResult> movieList, RecyclerView recyclerView) {
        this.context = context;
        this.movieList = movieList;

        if(recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = gridLayoutManager.getItemCount();
                    lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        //End has been reached, do something
//
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return movieList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @NonNull
    @Override
    public RecyclerViewMovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view,null);
        RecyclerViewMovieHolder holder = new RecyclerViewMovieHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewMovieHolder holder, int position) {
        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w185" + movieList.get(position).getPosterPath())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.movie_blank_portrait)
                        .error(R.drawable.movie_blank_portrait))
                .into(holder.image);
        holder.title.setText(movieList.get(position).getTitle());
        holder.rating.setText(movieList.get(position).getVoteAverage().toString() + " / 10");

        holder.original_title = movieList.get(position).getOriginalTitle();
        holder.id_film = movieList.get(position).getId().toString();
        holder.poster = "https://image.tmdb.org/t/p/w185" + movieList.get(position).getPosterPath();
        holder.backdrop = "https://image.tmdb.org/t/p/w780" + movieList.get(position).getBackdropPath();
        holder.release = movieList.get(position).getReleaseDate();
        holder.vote = movieList.get(position).getVoteAverage().toString();
        holder.vote_count = movieList.get(position).getVoteCount().toString();
        holder.overview = movieList.get(position).getOverview();

        HashMap<Integer,String> mapGenre = GenresData.getListGenres();
        int genre_id; String nameGenre; String prefix = "";
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<movieList.get(position).getGenreIds().size();i++){
            genre_id = movieList.get(position).getGenreIds().get(i);
            nameGenre = mapGenre.get(genre_id);
            sb.append(prefix);
            prefix = ", ";
            sb.append(nameGenre);
        }
        holder.genre = sb.toString();


    }

    @Override
    public int getItemCount() {
        return this.movieList.size();
    }

    public void setLoaded() {
        loading = false;
    }


    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
}
