package com.zidi.whatsmovie.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zidi.whatsmovie.R;
import com.zidi.whatsmovie.holder.RecyclerViewGenreHolder;

import java.util.List;

public class RecyclerViewGenreAdapter extends RecyclerView.Adapter<RecyclerViewGenreHolder> {

    private Context context;
    private List<String> genreList;
    private List<String> genreCodeList;

    public RecyclerViewGenreAdapter(Context context, List<String> genreList, List<String> genreCodeList) {
        this.context = context;
        this.genreList = genreList;
        this.genreCodeList = genreCodeList;
    }

    @NonNull
    @Override
    public RecyclerViewGenreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discover,null);
        RecyclerViewGenreHolder holder = new RecyclerViewGenreHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewGenreHolder holder, int position) {
        holder.mGenre.setText(genreList.get(position));
        holder.codeGenre = genreCodeList.get(position);

        //TODO : set DrawableLeft Icon
        switch (position){
            case 0:
                holder.mIcon.setImageResource(R.mipmap.action);
                break;
            case 1:
                holder.mIcon.setImageResource(R.mipmap.adventure);
                break;
            case 2:
                holder.mIcon.setImageResource(R.mipmap.animation);
                break;
            case 3:
                holder.mIcon.setImageResource(R.mipmap.comedy);
                break;
            case 4:
                holder.mIcon.setImageResource(R.mipmap.crime);
                break;
            case 5:
                holder.mIcon.setImageResource(R.mipmap.documentary);
                break;
            case 6:
                holder.mIcon.setImageResource(R.mipmap.drama);
                break;
            case 7:
                holder.mIcon.setImageResource(R.mipmap.family);
                break;
            case 8:
                holder.mIcon.setImageResource(R.mipmap.fantasy);
                break;
            case 9:
                holder.mIcon.setImageResource(R.mipmap.historical);
                break;
            case 10:
                holder.mIcon.setImageResource(R.mipmap.horror);
                break;
            case 11:
                holder.mIcon.setImageResource(R.mipmap.music);
                break;
            case 12:
                holder.mIcon.setImageResource(R.mipmap.mystery);
                break;
            case 13:
                holder.mIcon.setImageResource(R.mipmap.romance);
                break;
            case 14:
                holder.mIcon.setImageResource(R.mipmap.sciencefiction);
                break;
            case 15:
                holder.mIcon.setImageResource(R.drawable.ic_tv);
                break;
            case 16:
                holder.mIcon.setImageResource(R.mipmap.thriller);
                break;
            case 17:
                holder.mIcon.setImageResource(R.mipmap.war);
                break;
            case 18:
                holder.mIcon.setImageResource(R.mipmap.western);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return this.genreList.size();
    }
}
