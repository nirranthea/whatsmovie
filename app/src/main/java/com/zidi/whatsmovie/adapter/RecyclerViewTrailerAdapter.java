package com.zidi.whatsmovie.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zidi.whatsmovie.R;
import com.zidi.whatsmovie.holder.RecyclerViewTrailerHolder;
import com.zidi.whatsmovie.model.Videos;

import java.util.List;

public class RecyclerViewTrailerAdapter extends RecyclerView.Adapter<RecyclerViewTrailerHolder> {

    public Context context;
    public List<Videos.Result> trailerList;

    public RecyclerViewTrailerAdapter(Context context, List<Videos.Result> trailerList){
        this.context = context;
        this.trailerList = trailerList;
    }

    @NonNull
    @Override
    public RecyclerViewTrailerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video,null);
        RecyclerViewTrailerHolder holder = new RecyclerViewTrailerHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewTrailerHolder holder, int position) {
        holder.tvTrailer.setText(trailerList.get(position).getName());
        holder.keyTrailer = trailerList.get(position).getKey();
    }

    @Override
    public int getItemCount() {
        return this.trailerList.size();
    }
}
